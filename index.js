const express = require("express");
const app = express();
const port = 8000;
const expressLayouts = require("express-ejs-layouts");
const morgan = require('morgan');

app.set("view engine", "ejs");
app.use(expressLayouts);
app.use(express.static('public'));
app.use(morgan("dev"));

app.get("/", (req, res) => {
	const mahasiswa = [
		{
			nama: "Adam",
			jurusan: "Tehnik informatika"
		}
	]
	res.render("index", {
		layout: "./layouts/base",
		nama: "adam",
		title: "Home",
		mahasiswa,
	});
});

app.get("/about", (req, res) => {
	res.render("about", {
		layout: "./layouts/base",
		title: "About"
	});
});

app.get("/contact", (req, res) => {
	res.render("contact", {
		layout: "./layouts/base",
		title: "Contact"
	});
});

app.use("/", (req, res) => {
	res.status(404);
	res.send("<h1>404 Not found</h1>")
});

app.listen(port, () => {
	console.log(`Server is running at http://localhost:${port}`);
})
